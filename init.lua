--[[
Main Neovim config file.

Just import all required submodules.
]]--
require("user.colorscheme")
require("user.options")
require("user.commands")
require("user.keybinding")
require("user.netrw")
require("user.plugins")
require("user.autocommands")
require("user.lsp")

