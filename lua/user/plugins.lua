--[[
Plugins configuration.

Defines used plugins. Then loads their configuration.
]]--

local Plug = vim.fn["plug#"]

vim.call("plug#begin", "~/.config/nvim/plugged")

-- Async linter
Plug("neomake/neomake", { ["for"] = {"php", "sh"} })
-- Statusline
Plug("nvim-lualine/lualine.nvim")
Plug("nvim-tree/nvim-web-devicons")
Plug("romgrk/barbar.nvim")
-- Git integration
Plug("tpope/vim-fugitive")
-- LSP plugins
Plug("neovim/nvim-lspconfig")
Plug("hrsh7th/cmp-nvim-lsp")
Plug("hrsh7th/cmp-buffer")
Plug("hrsh7th/cmp-path")
Plug("hrsh7th/nvim-cmp")
-- Commentary
Plug("tpope/vim-commentary")
-- Todos
Plug("nvim-lua/plenary.nvim")
Plug("folke/todo-comments.nvim")

-- Vinegar
Plug("tpope/vim-vinegar")

-- Just
Plug("NoahTheDuke/vim-just", { ["for"] = { "just" } })

-- Terraform
Plug("hashivim/vim-terraform", { ["for"] = { "terraform" } })

-- TOML
Plug("cespare/vim-toml")

-- Colorscheme
Plug("morhetz/gruvbox")

-- VimPDB
Plug("gotcha/vimpdb", { ["for"] = {"python"} })

-- Snippets
Plug("SirVer/ultisnips", { ["for"] = {"cpp", "lua", "python", "rust", "tex"} })
Plug("honza/vim-snippets", { ["for"] = {"cpp", "lua", "python", "rust", "tex"} })

Plug("nvim-treesitter/nvim-treesitter", { ["do"] = ":TSUpdate" })

-- File explorer
Plug("kyazdani42/nvim-tree.lua")

-- ChatGPT integration
Plug("MunifTanjim/nui.nvim")
Plug("nvim-lua/plenary.nvim")
Plug("nvim-telescope/telescope.nvim")
Plug("jackMort/ChatGPT.nvim")

vim.call("plug#end")

require("user.plugins.lualine")
require("user.plugins.barbar")
require("user.plugins.neomake")
require("user.plugins.treesitter")
require("user.plugins.nvim_tree")
require("user.plugins.todo_comments")
if os.getenv("OPENAI_API_KEY") and os.getenv("OPENAI_API_KEY"):len() > 0 then
  require("user.plugins.chatgpt")
else
  print("OPENAI_API_KEY not defined, ChatGPT won't work")
end
