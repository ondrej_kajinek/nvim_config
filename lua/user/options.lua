--[[
Noevim global configuration.
]]--

local options = {
  -- Apperarance
  cursorline = true,  -- highlight current line
  lazyredraw = true,  -- don't redraw when executing macros
  linebreak = true,  -- wrap 'smartly', defined by 'breakat', e.g., break at word border
  mouse = "nv",  -- enable mouse for normal and visual mode -- insert breaks pasting :(
  number = true,  -- show line numbers
  numberwidth = 5,  -- minimal width of number lines
  scrolloff = 2,  -- how many lines to show above and below current line
  showmatch = true,  -- blink to matching parenthesis and back
  showmode = false,  -- don't show mode, since it is displayed by airline
  termguicolors = true,  -- enable 24 bit TUI color

  -- Buffer behaviour
  bufhidden = "hide",  -- don't unload buffer on close
  hidden = true,  -- don't force buffer saving on leaving it (e.g., bnext)

  -- Folding
  foldcolumn = "5",  -- width of fold column
  foldmethod = "expr",  -- fold by expression
  foldexpr = "nvim_treesitter#foldexpr()",  -- folding expression taken from treesitter
  foldopen = vim.opt.foldopen + "insert,jump", -- which action open fold
  foldlevelstart = -1,  -- initial fold level

  -- Indentation

  -- I/O
  confirm = true,  -- ask for action confirmation when neccessary (i.e. :q on modified buffer)
  fileformats= {"unix", "mac", "dos"},  -- priority or fileformats (i.e. EOL settings)
  swapfile = false,  -- don't use swapfile

  -- Search
  gdefault = true,  -- when searching/substituting, affect all appearances in line (same as s;;;g)
  ignorecase = true,  -- case-insensitive search
  smartcase = true,  -- use case-sensitive search when using uppercase chars

  -- Spell checking
  spell = true,  -- spell checking
  spelllang = {"cs", "en_gb"},  -- used languages

  -- Tabs vs spaces
  expandtab = true,  -- don't use tabs
  shiftwidth = 4,
  softtabstop = 4,
  tabstop = 4,

  -- Tab filename completion
  wildignore = vim.opt.wildignore + table.concat(
    {
      -- backup files, swap files
      "*~", "*.swp",
      -- compiled C / C++ files
      "*.o",
      -- compiled python files, python cache
      "*.pyc", "*.pyo", "*__pycache__", "*__pycache__/",
      -- LaTeX files
      "*.aux", "*.fls", "*.out", "*.pdf", "*.snm", "*.snr", "*.nav", "*.toc",
      -- localization files
      "*.mo",
    },
    ","
  ),
  wildmode = "longest:full",

  -- Undo
  undofile = true,  -- store undo changes per editted file

  -- TODO: sort
  autochdir = true,  -- auto-change to directory of current buffer
  matchpairs = vim.opt.matchpairs + "<:>",  -- pairs to jump with % key
  completeopt = {"menu", "menuone", "noselect"},
}

for key, value in pairs(options) do
  vim.opt[key] = value
end

vim.opt.formatoptions = vim.opt.formatoptions
  - "t"  -- don't autowrap using textwidth
  - "o"  -- don't insert comment when starting mode with o, O
  + "r"  -- continue in comment when hitting <Enter> in Insert mode
  + "l"  -- don't break long lines in insert mode

-- setup python3 and virtualenv
local python3_virtualenv = os.getenv("HOME") .. "/.config/nvim/venv/"
vim.g.python3_virtualenv = python3_virtualenv
vim.g.python3_host_prog = python3_virtualenv .. "bin/python"

vim.g.pyindent_open_paren = "&sw"
vim.g.pyindent_nested_paren = "&sw"
vim.g.pyindent_continue = "&sw"
