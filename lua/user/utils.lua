--[[
Useful functions for configuring NeoVim
]]--
local function buf_keymap(mode, key, action, bufnr)
  vim.api.nvim_buf_set_keymap(bufnr or 0, mode, key, action, { noremap = true })
end

local function buf_keymap_silent(mode, key, action, bufnr)
  vim.api.nvim_buf_set_keymap(bufnr or 0, mode, key, action, { noremap = true, silent = true })
end

local function keymap(mode, key, action)
  vim.api.nvim_set_keymap(mode, key, action, { noremap = true })
end

local function keymap_silent(mode, key, action)
  vim.api.nvim_set_keymap(mode, key, action, { noremap = true, silent = true })
end

return {
  buf_keymap = buf_keymap,
  buf_keymap_silent = buf_keymap_silent,
  keymap = keymap,
  keymap_silent = keymap_silent,
}
