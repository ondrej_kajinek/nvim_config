--[[
Custom commands.
]]--

-- clear search
vim.api.nvim_create_user_command(
  "CS",
  "let @/ = ''",
  { bang = true }
)

-- reload whole neovim configuration.
vim.api.nvim_create_user_command(
  "ReloadConfig",
  function()
    for name,_ in pairs(package.loaded) do
      if name:match("^user") then
        package.loaded[name] = nil
      end
    end

    dofile(vim.env.MYVIMRC)
  end,
  { bang = true }
)
