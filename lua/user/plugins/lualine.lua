--[[
Configuration for statusline plugin.
]]--

local Lualine = require("lualine")

local function highlight_group()
  return vim.cmd([[synIDattr(synID(line("."), col("."), 1), "name")]])
end

Lualine.setup(
  {
    options = {
      icons_enabled = true,
      theme = "auto",
      component_separators = { left = "", right = ""},
      section_separators = { left = "", right = ""},
      disabled_filetypes = {},
      always_divide_middle = true,
      globalstatus = false,
    },
    sections = {
      lualine_a = {"mode"},
      lualine_b = {
        "branch",
        "diff",
        "diagnostics",
      },
      lualine_c = {
        {
          "filename",
          file_status = true,
          newfile_status = true,
          path = 3,
          symbols = {
            modified = " [+]",
            readonly = " [-]",  -- add lock icon
            unnamed = " [No Name]",
            newfile = " [New]",
          },
        },
        -- {
        -- 	"%M",
        -- 	cond = function()
        -- 		return vim.bo.modified
        -- 	end,
        -- },
        {
          "%w",
          cond = function()
            return vim.wo.previewwindow
          end,
        },
        -- {
        -- 	"%r",
        -- 	cond = function()
        -- 		return vim.bo.readonly
        -- 	end,
        -- },
        {
          "%q",
          cond = function()
            return vim.bo.buftype == "quickfix"
          end,
        },
      },
      lualine_x = {"encoding", "fileformat", "filetype"},
      lualine_y = {"progress"},
      lualine_z = {
        "location",
        -- highlight_group,  -- show highlight group in statusline
      }
    },
    inactive_sections = {
      lualine_a = {},
      lualine_b = {},
      lualine_c = {"filename"},
      lualine_x = {"location"},
      lualine_y = {},
      lualine_z = {}
    },
    tabline = {},
    extensions = {},
  }
)
