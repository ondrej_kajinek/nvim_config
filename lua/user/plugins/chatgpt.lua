--[[
Configuration for ChatGTP.nvim plugin.
]]--

local ChatGPT = require("chatgpt")

local configuration = {
  openai_params = {
    model = "gpt-4o",
  },
  openai_edit_params = {
    model = "gpt-4o",
  },
}
ChatGPT.setup(configuration)

-- disable folding in the ChatGTP window
vim.api.nvim_create_autocmd(
  "BufWinEnter",
  {
    pattern = {"*"},
    group = vim.api.nvim_create_augroup("chatgpt_nvim", {clear = true}),
    desc = "Disable folding in ChatGPT window",
    callback = function(ev)
      if vim.bo.filetype == "markdown" then
        vim.wo.foldmethod = "manual"
      end
    end,
  }
)
