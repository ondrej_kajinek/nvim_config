--[[
Configuration for bufline plugin.
]]--
local Bufferline = require("bufferline")
local Utils = require("user.utils")

Bufferline.setup(
  {
    animation = false,
    clickable = false,
    icons = {
      filetype = {
        enabled = false,
      },
      button = false,
      separator = {
        left = "",
      },
      modified = {
        button = false,
      },
      inactive = {
        separator = {
          left = "",
        },
      },
    },
  }
)

-- Move to previous/next
Utils.keymap("n", "<C-h>", ":BufferPrevious<CR>")
Utils.keymap("n", "<C-l>", ":BufferNext<CR>")

-- Re-order to previous/next
-- Utils.keymap("n", "<A-<>", ":BufferMovePrevious<CR>")
-- Utils.keymap("n", "<A->>", " :BufferMoveNext<CR>")
Utils.keymap("n", "<A-h>", ":BufferMovePrevious<CR>")
Utils.keymap("n", "<A-l>", ":BufferMoveNext<CR>")

-- Goto buffer in position...
-- Utils.keymap("n", "<C-1>", ":BufferGoto 1<CR>")
-- Utils.keymap("n", "<C-2>", ":BufferGoto 2<CR>")
-- Utils.keymap("n", "<C-3>", ":BufferGoto 3<CR>")
-- Utils.keymap("n", "<C-4>", ":BufferGoto 4<CR>")
-- Utils.keymap("n", "<C-5>", ":BufferGoto 5<CR>")
-- Utils.keymap("n", "<C-6>", ":BufferGoto 6<CR>")
-- UtilUtils.keymap("n", "<C-7>", ":BufferGoto 7<CR>")
-- Utils.keymap("n", "<C-8>", ":BufferGoto 8<CR>")
-- Utils.keymap("n", "<C-9>", ":BufferGoto 9<CR>")
-- Utils.keymap("n", "<C-0>", ":BufferLast<CR>")

-- Pin/unpin buffer
Utils.keymap("n", "<A-p>", ":BufferPin<CR>")

-- Close buffer
Utils.keymap("n", "<leader>bw", ":BufferClose<CR>")
Utils.keymap("n", "<leader>BW", ":BufferCloseAllButCurrent<CR>")

-- Wipeout buffer
--                 :BufferWipeout<CR>
-- Close commands
--                 :BufferCloseAllButCurrent<CR>
--                 :BufferCloseAllButPinned<CR>
--                 :BufferCloseAllButCurrentOrPinned<CR>
--                 :BufferCloseBuffersLeft<CR>
--                 :BufferCloseBuffersRight<CR>

-- Magic buffer-picking mode
Utils.keymap("n", "<C-p>", ":BufferPick<CR>")

-- Sort automatically by...
-- Utils.keymap("n", "<leader>bb", ":BufferOrderByBufferNumber<CR>")
-- Utils.keymap("n", "<leader>bd", ":BufferOrderByDirectory<CR>")
-- Utils.keymap("n", "<leader>bl", ":BufferOrderByLanguage<CR>")
-- Utils.keymap("n", "<leader>bw", ":BufferOrderByWindowNumber<CR>")
