--[[
Configuration for NVim Tree plugin.
]]--

local NvimTree = require("nvim-tree")
local NvimTreeApi = require("nvim-tree.api")
local Utils = require("user.utils")

--- Open nvim tree on vim startup
-- When opening neovim with existing file or just with empty buffer, also open nvim tree (defocused)
local function nvim_tree_startup(data)
  local real_file = vim.fn.filereadable(data.file) == 1
  local empty_buffer = data.file == "" and vim.bo[data.buf].buftype == ""
  if real_file or empty_buffer then
    NvimTreeApi.tree.toggle(
      {
        focus = false,
        find_file = true,
      }
    )
  end
end

NvimTree.setup()  -- just use default configuration

Utils.keymap("n", "<leader>-", ":NvimTreeToggle<CR>")

vim.api.nvim_create_autocmd(
  { "VimEnter" },
  {
    callback = nvim_tree_startup,
  }
)

vim.api.nvim_create_autocmd(
  "BufEnter",
  {
    group = vim.api.nvim_create_augroup("NvimTreeClose", { clear = true }),
    callback = function()
      local layout = vim.api.nvim_call_function("winlayout", {})
      if layout[1] == "leaf" and vim.api.nvim_buf_get_option(vim.api.nvim_win_get_buf(layout[2]), "filetype") == "NvimTree" and layout[3] == nil then vim.cmd("quit") end
    end
  }
)
