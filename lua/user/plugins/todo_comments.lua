--[[
Configuration for Todo comments plugin.
]]--

local Todo = require("todo-comments")

Todo.setup(
  {
    search = {
      command = "grep",
      args = {
        "--recursive",
        "--color=never",
        "--with-filename",
        "--line-number",
        "--extended-regexp",
        "--binary-files=without-match",
      },
    }
  }
)
