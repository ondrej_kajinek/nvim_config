--[[
Configuration for Neomake plugin.
]]--

vim.g.neomake_logfile = "/tmp/neomake.log"
vim.g.neomake_open_list = 2
vim.g.neomake_list_height = 6
vim.g.neomake_error_sign = {
  text = "✖",
  texthl = "NeomakeErrorSign",
}
vim.g.neomake_warning_sign = {
  text = "!",
  texthl = "NeomakeWarningSign",
}
vim.g.neomake_info_sign = {
  text = "i",
  texthl = "NeomakeInfoSign",
}
vim.g.neomake_message_sign = {
  text = "m",
  texthl = "NoemakeMessageSign",
}

vim.g.neomake_php_enabled_makers = {"phpcs"}

vim.g.neomake_sh_enabled_makers = {"sh"}
vim.g.neomake_sh_sh_errorformat = table.concat(
  {
    "%E%f: line %l: %m",
    "%E%f: řádek %l: %m",
    "%E%f: %l: %m",
  },
  ","
)
