--[[
Global keybinds for vanilla neovim actions.
]]--

local Utils = require("user.utils")

vim.g.mapleader = ","

-- Autocomplete shortcuts
Utils.keymap("i", "<C-f>", "<C-x><C-f>")  -- Use C-f to complete by full line match

-- Copy, cut / paste to/from system clipboard
Utils.keymap("n", "yy", '"+yy')  -- copy whole line to system clipboard
Utils.keymap("n", "yx", '"+x')  -- cut one char to system clipboard
Utils.keymap("v", "y", '"+y')  -- copy selection to system clipboard
Utils.keymap("v", "x", '"+x')  -- cut selection to system clipboard
Utils.keymap("n", "p", '"+p')  -- paste from system clipboard
Utils.keymap("n", "P", '"+P')  -- paste from system clipboard

-- Disable F1 help, in all cases hit by accident (instead of Esc)
Utils.keymap("", "<F1>", "<nop>")
Utils.keymap("i", "<F1>", "<nop>")
Utils.keymap("", "<A-F1>", "<nop>")
Utils.keymap("i", "<A-F1>", "<nop>")

-- Fix syntax highlight
-- Utils.keymap("n", "<leader>fs", ":syntax sync fromstart<CR>")

-- Quick saving, quitting, opening, etc.
Utils.keymap("n", "<leader>e", ":e<CR>")  -- reload file
Utils.keymap("n", "<leader>n", ":edit ")  -- open file
Utils.keymap("n", "<leader>o", ":! xdg-open %<CR>")  -- open file in external viewer (xdg-open)
Utils.keymap("n", "<leader>O", ":! xdg-open %:r.")
Utils.keymap("n", "<leader>q", ":q<CR>")  -- close
Utils.keymap("n", "<leader>Q", ":qa<CR>")  -- close all
Utils.keymap("n", "<leader>t", ":enew<CR>")  -- open new file
Utils.keymap("n", "<leader>w", ":w<CR>")  -- save file
Utils.keymap("n", "<leader>x", ":x<CR>")  -- save file, close

-- Session management
Utils.keymap("n", "<leader>ss", ":mksession! ~/.config/nvim/sessions/nvim_session")
Utils.keymap("n", "<leader>ls", ":source ~/.config/nvim/sessions/nvim_session")
Utils.keymap("n", "<leader>rs", ":! rm ~/.config/nvim/sessions/nvim_session")

-- Reload ~/.config/nvim/init.lua
Utils.keymap_silent("n", "<leader>se", ":edit ~/.config/nvim/init.lua<CR>")
Utils.keymap_silent("n", "<leader>so", ":ReloadConfig<CR>")

-- Magic search
Utils.keymap("n", "/", "/\\v")
Utils.keymap("v", "/", "/\\v")

-- Execute . on each line in visual selection
Utils.keymap("v", ".", ":normal .<CR>")

-- cd to directory of current file
Utils.keymap("n", "<leader>cd", ":lcd %:p:h")

-- Fold mappings
Utils.keymap("n", "<leader>z0", ":setlocal foldlevel=0<CR>")
Utils.keymap("n", "<leader>z1", ":setlocal foldlevel=1<CR>")
Utils.keymap("n", "<leader>z2", ":setlocal foldlevel=2<CR>")
Utils.keymap("n", "<leader>z3", ":setlocal foldlevel=3<CR>")
Utils.keymap("n", "<leader>z4", ":setlocal foldlevel=4<CR>")
