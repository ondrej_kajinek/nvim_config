--- Configuration of Language Server Protocol integration.
-- @module lsp
-- Everything about making LSP work in neovim is defined here.
-- Selecting servers, setting them up, preparing keybinds,
-- completions, etc.
local Cmp = require("cmp")
local CmpLsp = require("cmp_nvim_lsp")
local LspConfig = require("lspconfig")

local diagnoctics_config = {
  severity_sort = true,
}

--- Toggle diagnostics
-- Switch diagnoctics on/off. This should help with viewing code that doesn't follow coding styles. Mostly useful
-- during candidate code review.
local function toggle_diagnostics()
  if vim.g.diagnostics_active then
    vim.g.diagnostics_active = false
    vim.diagnostic.hide()
  else
    vim.g.diagnostics_active = true
    vim.diagnostic.config(diagnoctics_config)
  end
end

--- Setup LSPs
-- Each server configuration is located in separate module.
-- Each lsp configuraiton module should return a table containing
-- all parameters to be passed to `setup` function.
local function setup_servers()
  local capabilities = CmpLsp.default_capabilities()
  local servers = {
    pylsp = require("user.lsp.pylsp"),
    clangd = require("user.lsp.clangd"),
    lua_ls = require("user.lsp.lua_lsp"),
    rust_analyzer = require("user.lsp.rust_analyzer"),
  }
  for server_name, server_args in pairs(servers) do
    local setup_args = {
      capabilities = capabilities,
    }
    for key, value in pairs(server_args) do
      setup_args[key] = value
    end
    LspConfig[server_name].setup(setup_args)
  end
end

--- Setup completion provided by LSPs.
-- Define keymaps used for competion box navigation and
-- define sources from which the completion hint are taken.
local function setup_completion()
  local completion_setup = {
    snippet = {
      expand = function(args)
        vim.fn["UltiSnips#Anon"](args.body)
      end
    },
    window = {
      completion = Cmp.config.window.bordered(),
      documentation = Cmp.config.window.bordered()
    },
    mapping = Cmp.mapping.preset.insert({
      ["<C-p>"] = Cmp.mapping.select_prev_item(),
      ["<C-n>"] = Cmp.mapping.select_next_item(),
      ["<C-d>"] = Cmp.mapping.scroll_docs(-4),
      ["<C-f>"] = Cmp.mapping.scroll_docs(4),
      ["<C-Space>"] = Cmp.mapping.complete(),
      ["<C-e>"] = Cmp.mapping.close(),
      ["<CR>"] = Cmp.mapping.confirm(
        {
          behavior = Cmp.ConfirmBehavior.Insert,
          select = true,
        }
      ),
    }),
    sources = Cmp.config.sources(
      {
        {
          name = "nvim_lsp",
          keyword_length = 1,
        },
        {
          name = "buffer",
          keyword_length = 3,
        },
        {
          name = "path",
          keyword_length = 3,
        },
      }
    ),
  }
  Cmp.setup(completion_setup)
end

--- Setup buffer keymaps for functionality provided by LSP.
local function setup_keymap()
  vim.api.nvim_create_autocmd(
    "LspAttach",
    {
      group = vim.api.nvim_create_augroup("UserLspConfig", {}),
      callback = function(ev)
        -- Enable completion triggered by <c-x><c-o>
        vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

        local opts = {
          buffer = ev.buf,
        }
        -- Mappings
        -- See `:help vim.lsp.*` for documentation on any of the below functions

        -- Go to ...
        vim.keymap.set("n", "<leader>gD", vim.lsp.buf.declaration, opts)
        vim.keymap.set("n", "<leader>gd", vim.lsp.buf.definition, opts)
        vim.keymap.set("n", "<leader>gi", vim.lsp.buf.implementation, opts)
        vim.keymap.set("n", "<leader>gr", vim.lsp.buf.references, opts)
        vim.keymap.set("n", "<leader>D",	vim.lsp.buf.type_definition, opts)

        -- Refactoring
        vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)
        vim.keymap.set("n", "<leader>f", function() vim.lsp.buf.format({async = true}) end, opts)

        -- Diagnostics
        vim.keymap.set("n", "<leader>ds", vim.diagnostic.open_float, opts)
        vim.keymap.set("n", "<leader>dn", vim.diagnostic.goto_next, opts)
        vim.keymap.set("n", "<leader>dp", vim.diagnostic.goto_prev, opts)

        -- Docs viewer
        vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
        vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, opts)

        vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, opts)

        -- Toggle diagnostics
        vim.keymap.set("n", "<leader>tt", toggle_diagnostics,  opts)
      end,
    }
  )
end

--- Setup diagnoctics
-- Diagnostic messages are sorted by their severity, showing the
-- most severe first.
local function setup_diagnostics()
  vim.g.diagnostics_active = true
  vim.diagnostic.config(diagnoctics_config)
end

setup_completion()
setup_servers()
setup_keymap()
setup_diagnostics()
