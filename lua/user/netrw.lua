--[[
Netrw configuration.
]]--

vim.g.netrw_browse_split = 4
vim.g.netrw_liststyle = 3
-- vim.g.netrw_list_hide = {".*\.swp$", ".*\.pyc$"}  -- TODO: dunno why this is not working
vim.g.netrw_winsize = 15
