--- Configuration for pylsp, python LSP.
-- @module pylsp
-- Enable used plugins and configure them.
-- Python binary from virtualenv used for Neovim is used.
-- See https://github.com/python-lsp/python-lsp-server for more details.

local settings = {
  pylsp = {
    configurationSources = {"flake8"},
    plugins = {
      flake8 = {
        enabled = false,
      },
      mccabe = {
        enabled = false,
      },
      pycodestyle = {
        enabled = false,
      },
      pyflakes = {
        enabled = false,
      },
      pylsp_mypy = {
        enabled = true,
        overrides = {
          "--python-executable", vim.api.nvim_eval("g:python3_host_prog"),
          "--no-implicit-optional",
          "--warn-unreachable",
          true,
        },
        strict = true,
      },
      pylint = {
        args = {
          "--disable=import-error,line-too-long,unused-argument",
          "--check-quote-consistency=y",
        },
        enabled = true,
      },
      ruff = {
        enabled = true,
        extendIgnore = {
          "A003",  -- builtin-attribute-shadowing
          "D105",  -- undocumented-magic-method
          "D107",  -- undocumented-public-init
          "D203",  -- one-blank-line-before-class
          "D213",  -- multi-line-summary-second-line
        },
        select = {
          "F",   -- flake8 / pyflakes
          "E",   -- flake8 / pycodestyle, errors
          "W",   -- flake8 / pycodestyle, warning
          "C90", -- McCabe
          "I",   -- isort
          "N",   -- pep8 naming, might already by checked by PyLint
          "D",   -- pydocstyle
          "UP",  -- pyupgrade
          "S",   -- bandit
          "BLE", -- blind exceptions
          "A",   -- builtins
          "T20", -- prints & pprints
          "ARG", -- unused args
          "PL",  -- pylint
          "RUF", -- ruff specific
        },
        lineLength = 120,
        perFileIgnores = {
          ["tests/**"] = {"S101"},
        }
      },
    },
  },
}

local pylsp = {
  cmd = { vim.g.python3_virtualenv .. "bin/pylsp" },
  settings = settings,
}
return pylsp
