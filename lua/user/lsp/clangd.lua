--- Configuration for clangd, C/C++ LSP.
-- @module clangd
-- Enable used plugins and configure them.

local clangd = {
  cmd = {
    "clangd",
    "--background-index",
    "--clang-tidy",
    "--clang-tidy-checks=*",
    "--completion-style=detailed",
    "--header-insertion-decorators",
    "--header-insertion=iwyu",
  },
}
return clangd
