--[[
Configuration for rust-analyzer, Rust LSP

See https://github.com/rust-lang/rust-analyzer/blob/master/docs/user/generated_config.adoc
for more details.

Rust-analyzer needs ``rust-src`` for autocomplete to work for std library.
]]--

local settings = {
  ["rust-analyzer"] = {
    imports = {
      granularity = {
        group = "module",
      },
      prefix = "self",
    },
    cargo = {
      buildScripts = {
        enable = true,
      },
    },
    check = {
      command = "clippy",
    },
    procMacro = {
      enable = true
    },
  },
}

local rust_analyzer = {
  settings = settings,
}

return rust_analyzer
