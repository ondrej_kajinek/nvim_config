--- Configuration for lua-language-server, Lua LSP.
-- @module lua_lsp
-- See https://github.com/sumneko/lua-language-server/wiki for more details.

local settings = {
  Lua = {
    runtime = {
      version = "Lua 5.2"
    },
    diagnostics = {
      globals = {
        "conky_parse",
        "vim",
      },
    },
    hint = {
      enabled = true,
    },
    type = {
      weakNilCheck = true,
      weakUnionCheck = true,
    },
    telemetry = {
      enabled = false,
    },
  },
}

local lua_lsp = {
  settings = settings,
}

return lua_lsp
