--[[
Autocommands definition.
]]--

local Utils = require("user.utils")

local function insert_cpp_guard()
  local gate_name = vim.fn.expand("%:t"):gsub("%.", "_"):upper()
  local skeleton = {
    "#ifndef " .. gate_name,
    "#define " .. gate_name,
    "",
    "",
    "",
    "#endif /* " .. gate_name .. " */",
    "",
  }
  vim.api.nvim_buf_set_lines(0, 0, 1, true, skeleton)
  vim.api.nvim_win_set_cursor(0, {4, 0})
end

local augroup = function(name)
  return vim.api.nvim_create_augroup(name, {clear = true})
end

-- Global
-- "	autoclose popup autocomplete
-- autocmd	InsertLeave,CompleteDone	*	if pumvisible() == 0 | silent! pclose | endif
vim.api.nvim_create_autocmd(
  "BufWritePre",
  {
    pattern = {
      -- C / C++
      "*.c", "*.cpp", "*.h", "*.hpp",
      -- html
      "*.html", "*.jinja", "*.twig",
      -- other programming languages
      "*.js", "*.lua", "*.php", "*.py",
      -- tex
      "*.tex",
    },
    desc = "Strip trailing spaces",
    command = ":%s;\\s\\+$;;e",
  }
)
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = {
      "apache",
      "cmake",
      "conf",
      "cpp",
      "dockerfile",
      "dosini",
      "gitignore",
      "haskell",
      "javascript",
      "lua",
      "matlab",
      "php",
      "python",
      "sh",
      "sql",
      "snippets",
      "sshconfig",
      "terraform",
      "vim",
      "yaml",
    },
    desc = "Disable spell checking",
    callback = function()
      vim.wo.spell = false
    end,
  }
)

-- File type specific
-- Bash
local bash_augroup = augroup("sh")
vim.api.nvim_create_autocmd(
  "BufNewFile",
  {
    pattern = {"*.bash", "*.sh"},
    group = bash_augroup,
    desc = "Insert skeleton for new shell scripts",
    command = "0r ~/.config/nvim/skeletons/skeleton.sh | normal! G",
  }
)
vim.api.nvim_create_autocmd(
  "BufWritePost",
  {
    pattern = {"*.bash", "*.sh"},
    group = bash_augroup,
    desc = "Neomake runner for shell scripts",
    command = "Neomake",
  }
)

-- CMake
local cmake_augroup = augroup("cmake")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = {"cmake"},
    group = cmake_augroup,
    desc = "CMake autocommands",
    callback = function()
      vim.bo.shiftwidth = 2
      vim.bo.tabstop = 2
    end,
  }
)

-- C++
local cpp_augroup = augroup("cpp")
vim.api.nvim_create_autocmd(
  "BufNewFile",
  {
    pattern = {"*.h", "*.hpp"},
    group = cpp_augroup,
    desc = "Insert include guard for C/C++ header file",
    callback = insert_cpp_guard,
  }
)
vim.api.nvim_create_autocmd(
  "BufNewFile",
  {
    pattern = {"*.c", "*.cpp"},
    group = cpp_augroup,
    desc = "Insert basic stuff to C/C++ source file",
    command = "0r ~/.config/nvim/skeletons/skeleton.c | normal! G",
  }
)
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = {"*.c", "*.cpp", "*.h", "*.hpp"},
    group = cpp_augroup,
    desc = "C/C++ autocommands",
    callback = function()
      vim.wo.colorcolumn = 120
      vim.bo.expandtab = true
      vim.bo.shiftwidth = 4
      vim.bo.softtabstop = 4
      vim.bo.tabstop = 4
    end,
  }
)

-- Dosini
local dosini_augroup = augroup("dosini")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "dosini",
    group = dosini_augroup,
    desc = "Dos INI basic settings",
    callback = function()
      -- foldmethod=syntax
      vim.wo.linebreak = true
      vim.wo.showbreak = ">\\"
      vim.wo.wrap = true
    end,
  }
)

-- Helm
local helm_augroup = augroup("helm")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "helm",
    group = helm_augroup,
    desc = "Helm autocommands",
    callback = function()
      vim.bo.expandtab = true
      vim.bo.shiftwidth = 2
      vim.bo.softtabstop = 2
      vim.bo.tabstop = 2
    end,
  }
)

-- HTML
local html_augroup = augroup("html")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "html",
    group = html_augroup,
    desc = "Helm autocommands",
    callback = function()
      vim.bo.expandtab = true
      vim.bo.smartindent = false
      vim.bo.shiftwidth = 2
      vim.bo.softtabstop = 2
      vim.bo.tabstop = 2
    end,
  }
)

-- Jinja
local jinja_augroup = augroup("jinja")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "jinja",
    group = jinja_augroup,
    desc = "Jinja autocommands",
    callback = function()
      vim.bo.expandtab = true
      vim.bo.smartindent = false
      vim.bo.shiftwidth = 2
      vim.bo.softtabstop = 2
      vim.bo.tabstop = 2
    end,
  }
)

-- Lua
local lua_augroup = augroup("lua")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "lua",
    group = lua_augroup,
    desc = "Lua basic settings",
    callback = function()
      vim.wo.colorcolumn = "120"
      vim.bo.expandtab = true
      vim.bo.shiftwidth = 2
      vim.bo.softtabstop = 2
      vim.bo.tabstop = 2
    end,
  }
)

-- PHP
local php_augroup = augroup("php")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "php",
    group = php_augroup,
    desc = "PHP basic settings",
    callback = function()
      -- TODO: what?
      vim.wo.colorcolumn = 86
      vim.bo.expandtab = true
      vim.bo.shiftwidth = 4
      vim.bo.softtabstop = 4
      vim.bo.tabstop = 4
    end,
  }
)
vim.api.nvim_create_autocmd(
  "BufNewFile",
  {
    pattern = "php",
    group = php_augroup,
    desc = "Insert skeleton for new PHP file",
    command = "0r ~/.config/nvim/skeletons/skeleton.php | normal! G",
  }
)

-- PostgreSQL
local psql_augroup = augroup("psql")
vim.api.nvim_create_autocmd(
  "BufEnter",
  {
    pattern = "*.psql",
    group = psql_augroup,
    desc = "PostgreSQL filetype fixer",
    callback = function()
      vim.bo.filetype = "sql"
    end,
  }
)

-- Python
local python_augroup = augroup("python")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "python",
    group = python_augroup,
    desc = "Python autocommands",
    callback = function()
      vim.wo.colorcolumn = "72,120"
      -- vim.bo.smartindent = false
      vim.bo.expandtab = true
      vim.bo.shiftwidth = 4
      vim.bo.softtabstop = 4
      vim.bo.tabstop = 4
      vim.bo.omnifunc = "syntaxcomplete#Complete"
    end,
  }
)

-- reStructuredText
local rst_augroup = augroup("rst")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "rst",
    group = rst_augroup,
    desc = "reStructuredText autocommands",
    callback = function()
      vim.bo.expandtab = true
      vim.bo.shiftwidth = 2
      vim.bo.softtabstop = 2
      vim.bo.tabstop = 2
    end,
  }
)

-- TeX
local tex_augroup = augroup("tex")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "plaintex",
    group = tex_augroup,
    desc = "PlainTeX filetype fixer",
    callback = function()
      vim.bo.filetype = "tex"
    end,
  }
)
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "tex",
    group = tex_augroup,
    desc = "TeX keymapping",
    callback = function()
      Utils.buf_keymap("n", "<leader>lm", ":w <BAR> ! xelatex %<CR>")
      Utils.buf_keymap("n", "<leader>lv", ":! xdg-open %:r.pdf<CR><CR>")
    end,
  }
)
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "tex",
    group = tex_augroup,
    desc = "TeX basic configuration",
    callback = function()
      vim.bo.tabstop = 2
      vim.bo.shiftwidth = 2
      -- vim.bo.iskeyword:append(":")
      -- vim.opt.wildignore:append("*.log")
    end,
  }
)

-- Terraform
local tf_augroup = augroup("tf")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "terraform",
    group = tf_augroup,
    desc = "Terraform autocommands",
    callback = function()
      vim.wo.foldmethod = "indent"
    end,
  }
)

-- Twig
local twig_augroup = augroup("twig")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "twig",
    group = twig_augroup,
    desc = "Twig basic settings",
    callback = function()
      vim.bo.expandtab = true
      vim.bo.shiftwidth = 2
      vim.bo.softtabstop = 2
      vim.bo.tabstop = 2
    end,
  }
)

-- YAML
local yaml_augroup = augroup("yaml")
vim.api.nvim_create_autocmd(
  "FileType",
  {
    pattern = "yaml",
    group = yaml_augroup,
    desc = "YAML basic settings",
    callback = function()
      vim.bo.expandtab = true
      vim.bo.shiftwidth = 2
      vim.bo.softtabstop = 2
      vim.bo.tabstop = 2
    end,
  }
)
