--[[
Colorscheme seting.

Currently using vimL.
]]--

local function colorscheme(name)
  local status_ok, error_message = pcall(vim.cmd, "colorscheme " .. name)
  if not status_ok then
    vim.notify("colorscheme: " .. name .. " failed with " .. error_message)
  end
end

colorscheme("gruvbox2")
