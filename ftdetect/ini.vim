" Figure out which type of hilighting to use for html.
fun! s:SelectINI()
	let n = 1
	while n < 50 && n <= line("$")
		" check for ini
		if getline(n) !~ '^\s*$\|^\[[^]]\+\]$\|^[^#=;]\+\s*=\s*.*$\|^;.*$\|^#.*$'
			return
		endif
		let n = n + 1
	endwhile
	set ft=dosini
endfun
autocmd BufNewFile,BufRead *.conf  call s:SelectINI()
